package org.ater.cheilman.isolation

/**
 * Contains the board spaces and their contents.
 *
 * Board array is indexed as board[row/y][column/x].
 */
public data class Board (val height : Int, val width : Int) {

    private val board : Array<Array<BoardContents>> = Array(height, { y -> Array(width, { x -> BoardContents.EMPTY} ) });
    private val playerPositions : MutableMap<String, Location> = hashMapOf();

    /**
     * Is a given location contained within the board.
     */
    fun isLocationValid(loc : Location) = (loc.row >= 0) && (loc.row < board.size()) &&
                                          (loc.column >= 0) && (loc.column < board.get(loc.row).size());

    /**
     * Return the contents of the space at the given location.
     */
    fun contents(loc : Location) = board.get(loc.row).get(loc.column);

    /**
     * Set the contents of the space at the given location.
     */
    protected fun contents(loc : Location, newContents : BoardContents) : Unit
            = board.get(loc.row).set(loc.column, newContents);

    /**
     * Update the player's location in our map.
     */
    protected fun updatePlayerLocation(player : String, loc : Location) {
        playerPositions.put(player, loc);
    }

    /**
     * Return the location for a given player.
     */
    fun findPlayer(id : String) = playerPositions.get(id);

    /**
     * List all valid moves from an initial space.
     */
    fun validMoves(start : Location) : Set<Location> {
        val retval : MutableSet<Location> = hashSetOf();

        for (dx in -1..1) {
            for (dy in -1..1) {
                if ((dx == 0) && (dy == 0)) {
                    continue;
                }

                var loc = start.copy(start.row + dy, start.column + dx);

                while ((isLocationValid(loc)) && (!contents(loc).isBlocked)) {
                    retval.add(loc);

                    loc = loc.copy(start.row + dy, start.column + dx)
                }
            }
        }

        return retval;
    }

    /**
     * List all valid moves from a player's current location.  If the player
     * does not have a current location, then we'll assume this is the first move
     * and allow any unoccupied space.
     */
    fun validMoves(playerId : String) : Set<Location> {
        val playerLoc = findPlayer(playerId);

        if (playerLoc != null) {
            return validMoves(playerLoc);
        } else {
            val retval : MutableSet<Location> = hashSetOf();

            for (y in 0..(height - 1)) {
                for (x in 0..(width - 1)) {
                    val loc = Location(y, x);

                    if (!contents(loc).isBlocked) {
                        retval.add(loc);
                    }
                }
            }

            return retval;
        }
    }

    /**
     * Execute the given move, returning a new board that represents the new state.
     */
    fun executeMove(player : String, newLocation : Location) : Board {
        // Validate move
        if (!validMoves(player).contains(newLocation)) {
            // Invalid move!
            throw IllegalStateException("Move " + newLocation + " is not valid for player " + player);
        }

        // Copy the board
        val newBoard = this.copy();

        // Update original location
        val origLocation = findPlayer(player);

        if (origLocation != null) {
            newBoard.contents(origLocation, BoardContents.FILLED_BY_PLAYER(player));
        }

        // Update new location
        newBoard.contents(newLocation, BoardContents.PLAYER(player));
        newBoard.updatePlayerLocation(player, newLocation);

        // Return
        return newBoard;
    }
}
