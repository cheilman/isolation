package org.ater.cheilman.isolation

/**
 * Stores a location on the game board.
 */
public data class Location(val row : Int, val column: Int) {

}
