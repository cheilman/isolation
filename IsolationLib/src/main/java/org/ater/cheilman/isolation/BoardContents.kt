package org.ater.cheilman.isolation

/**
 * What lies at a particular spot on the board.
 */
public data class BoardContents(val isBlocked : Boolean,
                                val isPlayer : Boolean,
                                val id : String) {
    class object {
        val EMPTY : BoardContents = BoardContents(false, false, "");
        fun PLAYER(id : String) = BoardContents(true, true, id);
        fun FILLED_BY_PLAYER(id : String) = BoardContents(true, false, id);
    }
}
