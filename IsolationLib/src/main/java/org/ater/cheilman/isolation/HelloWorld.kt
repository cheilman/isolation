package org.ater.cheilman.isolation

/**
 * Created by heilmanc on 3/10/15.
 */

class HelloWorld(val toWho : String) {
    fun message() : String {
        return "Hello, " + toWho + "!";
    }
}
