package org.ater.cheilman.isolation

import org.fest.assertions.api.Assertions.assertThat
import org.junit.Test

/**
 * Created by heilmanc on 3/10/15.
 */

public class HelloWorldTest {

    Test
    fun testMessage() {
        val hw = HelloWorld("World");

        assertThat(hw.message()).isNotNull().isNotEmpty().isEqualTo("Hello, World!");
    }
}
